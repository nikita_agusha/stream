import React from "react"
import man from "../../image/man.svg";
import ease from "../../image/black-eye.svg"
import people from "../../image/icon-people.svg"
import like from "../../image/like-2.svg"
import heart from "../../image/heart.svg"
import ReactPlayer from "react-player";
import ChatPlayerVideo from "../../components/chatPlayerVideo/chatPlayerVideo";

function PlayerVideo() {


    return (
        <div className="playerVideo">
            <div className="playerVideoBlock">
                <ReactPlayer
                    url='http://13.53.114.8:5080/LiveApp/streams/246843685823688878614960.m3u8?token=22e89fa93d0f1ff7732d3b83bc4a2b0c15973c54a3960454894378ce75b95709'
                    width="100%"
                    height="auto"
                    controls={true}/>
                <div className="playerVideoInfoBlock">
                    <div className="playerVideoInfo">
                        <img src={man} alt=""/>
                        <div className="playerVideoPeopleInfo">
                            <p className="playerVideoInfoText">Стрим по доте два, розыгрышь скинов </p>
                            <div className="playerVideoTagAndInfo">
                                <div>
                                    <p className="playerVideoInfoText"><span
                                        className="playerVideoInfoTextName">dmitriy_dota </span> стримит Dota 2</p>
                                </div>
                                <div className="videoTags">
                                    <p className="videoTag playerVideoTag">Русский</p>
                                    <p className="videoTag playerVideoTag">Moba</p>
                                    <p className="videoTag playerVideoTag">Русский</p>
                                    <p className="videoTag playerVideoTag">Moba</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="playerVideoBlockLiveWatch">
                        <div className="playerVideoBlockLiveWatchBlock">
                            <p><img src={people} alt=""/>8240</p>
                            <p><img src={ease} alt=""/>8240</p>
                        </div>
                        <div className="playerVideoBlockLiveWatchBlock">
                            <a href="#" className="playerVideoBlockLiveWatchButtonLike">
                                <img src={like} className="playerVideoBlockLiveWatchButtonLikeImg"
                                     alt=""/>Понравилось</a>
                            <a href="#" className="playerVideoBlockLiveWatchButtonSub">
                                <img src={heart} className="playerVideoBlockLiveWatchButtonLikeImg" alt=""/>Подписаться</a>
                        </div>

                    </div>
                </div>
            </div>
            <ChatPlayerVideo/>
        </div>
    )

}

export default PlayerVideo