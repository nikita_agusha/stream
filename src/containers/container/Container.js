import React from "react";
import PlayList from "../PlayList/PlayList";
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from "../footer/footer";

function Containers() {
    return (
        <div>
            <PlayList/>
            <Footer/>
        </div>
    )

}

export default Containers