import React, {useState} from "react";
import {
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
} from 'reactstrap';
import classnames from 'classnames';
import facebookImage from "../../image/facebook-Icon.svg"
import ease from "../../image/eye.svg"
import closeEase from "../../image/closeEase.png"
import axios from "axios";
import RecoveryPassword from "../recoveryPassword/recoveryPassword";
import {saveToLocalStorage} from "../../LocalStorage";


function SingInAuthorizationBlock(props) {
    const BASE_URL = window.env.ENV_VARIABLE;
    const [activeTab, setActiveTab] = useState(props.numberTab);
    const [userName, setUserName] = useState("")
    const [userPassword, setUserPassword] = useState("")
    const [passwordChange, setPasswordChange] = useState("")
    const [userEmail, setUserEmail] = useState("")
    const [formatPassword, setFormatPassword] = useState(false)
    const [error, setError] = useState("")
    const [blockProblemSingIn, setBlockProblemSingIn] = useState(false)


    const register = (e) => {
        e.preventDefault()
        if (userPassword !== passwordChange) {
            setError("Пароли не совпадают")
            return setTimeout(() => {
                setError("")
            }, 3000)

        }


        axios.post(BASE_URL + "/users/registration", {
            username: userName,
            password: userPassword,
            email: userEmail
        })
            .then(r => {
                console.log(r.data)
                logIn(e)
            })
            .catch(e => {
                setError(e.response)
                setTimeout(() => {
                    setError("")
                    setUserName(" ")
                    setUserPassword(" ")
                }, 3000)
            })
    }


    const toggle = tab => {
        if (activeTab !== tab) {
            setUserPassword("")
            setPasswordChange("")
            setActiveTab(tab);
        }
    }

    const changePassword = () => {
        if (!formatPassword) {
            setFormatPassword(true)
        } else {
            setFormatPassword(false)
        }
    }

    const logIn = (e) => {
        e.preventDefault();
        axios.post(BASE_URL + "/users/auth", {
            username: userName,
            password: userPassword
        }).then(r => {
            saveToLocalStorage(r.data)
            props.closeModal(false)
        })
            .catch(e => {
                setError(e.response)
                setTimeout(() => {
                    setError("")
                    setUserName(" ")
                    setUserPassword(" ")
                }, 3000)
            })
    }


    const problemSingIn = () => {
        if (!blockProblemSingIn) {
            setBlockProblemSingIn(true)
        } else {
            setBlockProblemSingIn(false)
        }
    }


    return (<>

            <div className="modalTab"
                 onClick={
                     () => props.toggleBlock()}>
            </div>
            <div className="singInBlock">


                <p className="welcome"> Добро пожаловать! </p>
                <Nav tabs>
                    <NavItem>
                        <NavLink className={classnames({active: activeTab === '1'})}
                                 onClick={
                                     () => {
                                         toggle('1');
                                     }
                                 }>
                            Войти </NavLink> </NavItem> <NavItem>
                    <NavLink className={classnames({active: activeTab === '2'})}
                             onClick={
                                 () => {
                                     toggle('2');
                                 }
                             }>
                        Зарегистрироваться </NavLink>
                </NavItem>
                </Nav>
                <TabContent activeTab={activeTab}>
                    <TabPane tabId="1"
                             className="sinInTab">
                        <div>
                            <h4> Имя пользователя </h4> <
                            input type="text"
                                  name="name"
                                  value={userName}
                                  onChange={e => setUserName(e.target.value)}/></div>
                        <div>
                            <h4> Пароль </h4>
                            <div>

                                <input name="password"
                                       value={userPassword}
                                       type={!formatPassword ? "password" : "text"}
                                       onChange={e => setUserPassword(e.target.value)}
                                /> <a href="#"
                                      onClick={changePassword}>{!formatPassword ? < img className="checkPasswordSingIn"
                                                                                        src={ease}
                                                                                        alt=""/> :
                                < img className="checkPasswordSingIn"
                                      src={closeEase}
                                      alt=""/>} </a>
                            </div>

                        </div>
                        <div>
                            <p className="singInSystemBlock" onClick={problemSingIn}
                            > Проблема со входом в систему ? </p>
                            <a href="#"
                               onClick={logIn} className="buttonEnter"> Войти </a>
                            <p className="singInSystemBlock" style={{padding: "17px 0 10px"}}> Или войти через : </p>
                            <img src={facebookImage} className="faceBookSingIn" alt=""/>
                        </div>

                    </TabPane>
                    <TabPane tabId="2"
                             className="sinInTab">
                        <div>
                            <h4> Имя пользователя </h4> <
                            input type="text"
                                  name="name"
                                  value={userName}
                                  onChange={e => setUserName(e.target.value)}/>
                        </div>
                        <div>
                            <h4> Пароль </h4>
                            <input type={!formatPassword ? "password" : "text"}
                                   name="password"
                                   value={userPassword}

                                   onChange={e => setUserPassword(e.target.value)}/>
                            <a href="#" onClick={changePassword}>
                                {!formatPassword ?
                                    <img className="checkPassword" src={ease} alt=""/>
                                    :
                                    < img className="checkPassword" src={closeEase} alt=""/>
                                }
                            </a>
                        </div>
                        <div>
                            <h4> Подтверждение пароля </h4> <
                            input type={!formatPassword ? "password" : "text"}
                                  name="passwordChange"
                                  value={passwordChange}
                                  onChange={e => setPasswordChange(e.target.value)}
                        /> <a href="#"
                              onClick={changePassword}>

                            {!formatPassword ?
                                <img className="checkPasswordDone" src={ease} alt=""/>
                                :
                                < img className="checkPasswordDone" src={closeEase} alt=""/>
                            }
                        </a></div>
                        <div>
                            <h4> Эл.адрес </h4> <
                            input type="email"
                                  name="email"
                                  value={userEmail}
                                  onChange={e => setUserEmail(e.target.value)}
                        /></div>
                        <div>
                            <p style={{
                                fontSize: "14px",
                                lineHeight: "17px",
                                margin: "38px 0 41px"
                            }}> Нажимая «Зарегистрироваться», вы подтверждаете, что прочли и в полной мере осознаете
                                <span style={{color: "#0A84FF"}}> условия обслуживания</span> и положения <span
                                    style={{color: "#0A84FF"}}> политики конфиденциальности. </span></p>

                            <a href="#"
                               onClick={register} className="buttonEnter"> Зарегистрироваться </a>
                            <p className="singInSystem"> Или зарегестироваться через: </p>
                            <img src={facebookImage} className="faceBookSingIn" alt=""/>
                        </div>

                    </TabPane> </TabContent>
                <div>
                    {blockProblemSingIn ? <RecoveryPassword closemodal={problemSingIn}/> : null}

                </div>
            </div>
        </>
    )
}

export default SingInAuthorizationBlock