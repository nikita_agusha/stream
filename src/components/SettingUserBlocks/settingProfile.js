import React, {useEffect, useState} from "react";
import image from "../../image/man.svg"
import axios from "axios";
import {loadFromLocalStorage, saveToLocalStorage} from "../../LocalStorage";
import {Redirect} from "react-router";
import ResetPassword from "../resetNewPassword/resetNewPassword";


function SettingProfile(props) {
    console.log(loadFromLocalStorage())
    const [userInfo, setUserInfo] = useState(" ")
    const BASE_URL = window.env.ENV_VARIABLE;
    const [token, setToken] = useState(loadFromLocalStorage().auth_token)
    const [showModal, setShowModal] = useState(false)
    const [newInfo, setNewInfo] = useState()

    useEffect(() => {
        axios.get(BASE_URL + "/users/me", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        ).then(r => {
            setUserInfo(r.data)
        })
    }, [])

    const onShowModal = () => {
        if (!showModal) {
            setShowModal(true)
        } else {
            setShowModal(false)
        }
    }




    const onFileChangeHandler = e => {
        const formData = new FormData();
        formData.append('file', e.target.files[0]);
        axios.put(BASE_URL + "/users/avatar",
            formData, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        ).then(r => {
            axios.get(BASE_URL + "/users/me", {
                headers:
                    {
                        Authorization: `Bearer ${token}`
                    }
            }).then(r => {
                setUserInfo(r.data)
            })
        })
    };

    const onChange = (e) => {
        e.preventDefault()
        setNewInfo({...newInfo, [e.target.name]: e.target.value})
        setUserInfo({...newInfo, [e.target.name]: e.target.value})
    }


    const editData = () => {
        axios.post(BASE_URL + "/users/me", newInfo, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        ).then(r => {
            getInfoUser(token)
        })
    }

    const getInfoUser = (token) => {
        if(newInfo.username){
            saveToLocalStorage("")
        }
        setUserInfo("")
        axios.get(BASE_URL + "/users/me", {
            headers:
                {
                    Authorization: `Bearer ${token}`
                }
        }).then(r => {
            setUserInfo(r.data)
        })
    }

    return (
        <div className="SettingProfile">
            <p className="SettingProfileTitle settingTitle">Настройки профиля</p>
            <div className="SettingProfileBlock">
                <div className="SettingProfileBlockPhoto">

                    <div className="SettingProfileBlockPhotoImage">
                        {
                            (userInfo.path_to_avatar !== null) ? <img src={userInfo.path_to_avatar} alt=""/> :
                                <img src={image} alt=""/>
                        }
                    </div>
                    <div className="SettingProfileBlockPhotoImageEdit">
                        <div className="input__wrapper">
                            <input name={userInfo.path_to_avatar}
                                   onChange={onFileChangeHandler} type="file"
                                   id="input__file"
                                   className="input input__file"
                                   multiple/>
                            <label htmlFor="input__file" className="input__file-button">
                                <span className="input__file-button-text SettingProfileBlockPhotoImageEditButton">Изменить фотографию</span>
                            </label>
                        </div>
                        <p className="SettingProfileBlockPhotoImageEditText">Поддерживаемые форматы: PNG, JPG или
                            GIF.</p>
                    </div>
                </div>
                <div className="SettingProfileBlockPhoto">
                    <div className="SettingProfileTitle SettingProfileTitleName">
                        <p>Имя пользователя</p>
                    </div>
                    <div className="SettingProfileBlockEdit">
                        <input type="text" className="SettingProfileBlockNameUser" placeholder="dmitriy_dota"
                               value={(userInfo.username)?userInfo.username:""}
                               name="username"
                               onChange={onChange}/>
                        <p className="SettingProfileBlockEditInfo">Вы можете обновить имя по своему желанию</p>
                    </div>
                </div>
                <div className="SettingProfileBlockPhoto">
                    <div className="SettingProfileTitle SettingProfileTitleName">
                        <p style={{lineHeight: "160%"}}>Электронная
                            почта</p>
                    </div>
                    <div className="SettingProfileBlockEdit">
                        <input type="email"
                               className="SettingProfileBlockNameUser"
                               name="email"
                               value={(userInfo.email)?userInfo.email:""}
                               onChange={onChange}
                               placeholder="dmitriy_dota@gmail.com"/>
                        <p className="SettingProfileBlockEditInfo">Вы можете обновить <span> изменить почту</span> по
                            своему желанию</p>


                    </div>
                </div>
                <div className="SettingProfileBlockPhoto">
                    <div className="SettingProfileTitle SettingProfileTitleName">
                        <p style={{lineHeight: "160%"}}>Изменить
                            пароль</p>
                    </div>
                    <div className="SettingProfileBlockEdit">
                        <p className="SettingProfileBlockEditInfo SettingProfileBlockEditInfoPassword">
                            <span onClick={onShowModal}> Измените пароль.</span> Повысьте безопасность, выбрав более
                            надежный пароль.
                        </p>
                    </div>
                </div>
                <div className="SettingProfileBlockPhoto">
                    <div className="SettingProfileTitle SettingProfileTitleName">
                        <p>Двухфакторная аутентификация</p>
                    </div>
                    <div className="SettingProfileBlockEdit">
                        <p className="SettingProfileBlockEditPlugAuth">Подключить</p>
                        <p className="SettingProfileBlockEditInfo ">
                            При входе на наш сервис, вам будет приходить дополнительный код
                        </p>
                    </div>
                </div>
            </div>
            <div className="saveNewData">
                <p onClick={editData}>Сохранить изменения</p>
            </div>
            {(showModal) ? <ResetPassword onChange={onShowModal}/> : null}
        </div>
    )
}

export default SettingProfile