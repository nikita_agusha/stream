import React, {useState} from "react";
import axios from "axios";
import {loadFromLocalStorage, saveToLocalStorage} from "../../LocalStorage";
import {NotificationManager} from "react-notifications";



const ResetPassword = (props) => {
    const [passwords, setPasswords]= useState({password:"", confirmPassword:""})
    const BASE_URL = window.env.ENV_VARIABLE;
    const [token, setToken] = useState(loadFromLocalStorage().auth_token)


    const onChangePasswords=(e)=>{
        e.preventDefault()
        setPasswords({...passwords,[e.target.name]:e.target.value})

    }
    const saveNewPassword=()=>{
        axios.post(BASE_URL +"/users/me",passwords,{
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(r=>{
            saveToLocalStorage({auth_token:r.data.message})
            setToken(loadFromLocalStorage().auth_token)
            NotificationManager.success("password reset")
            props.onChange()
            window.location.reload();
            console.log(r.data)

        })
    }
    console.log(passwords)
    return (
        <>
            <div className="modalTab" onClick={() => {
                props.onChange()
            }}>

                <div className="recoveryBlock resetPasswordBlock" onClick={(e) => e.stopPropagation()}>
                        <p className="resetPasswordBlockTitle">Смена пароля</p>
                    <p>Новый пароль </p>
                    <input type="text" name="password" onChange={onChangePasswords} value={passwords.password}/>
                    <p>Повторите новый пароль</p>
                    <input type="text" name="confirmPassword"  onChange={onChangePasswords}  value={passwords.confirmPassword}/>
                    <a href="#"
                       className="recoveryButton" onClick={saveNewPassword}> Сменить пароль</a>
                </div>
            </div>
        </>
    )
}
export default ResetPassword