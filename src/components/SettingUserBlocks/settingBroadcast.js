import React, {useEffect, useState} from "react";
import search from "../../image/search.svg";
import quest from "../../image/quest.svg";
import axios from "axios";
import {loadFromLocalStorage} from "../../LocalStorage";
import {Input} from "reactstrap";


function SettingBroadcast(props) {
    const [userInfo, setUserInfo] = useState("")
    const [token, setToken] = useState(loadFromLocalStorage().auth_token)
    const BASE_URL = window.env.ENV_VARIABLE;

    const onChange = (e) => {
        e.preventDefault()
        setUserInfo({...userInfo, [e.target.name]: e.target.value})
    }


    useEffect(() => {
        axios.get(BASE_URL + "/users/me", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        ).then(r => {
            console.log(r.data)
            setUserInfo(r.data.broadcast)

        })
    }, [])
    const onFileChangeHandler = e => {
        const formData = new FormData();
        formData.append('file', e.target.files[0]);
        axios.put(BASE_URL + "/users/avatar",
            formData, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        ).then(r => {
            axios.get(BASE_URL + "/users/me", {
                headers:
                    {
                        Authorization: `Bearer ${token}`
                    }
            }).then(r => {
                setUserInfo(r.data)
            })
        })
    };
    const resetData = (e) => {
        setUserInfo(userInfo.stream_key = "")
    };

    console.log(userInfo)
    return (
        <div className="SettingProfile">
            <p className="SettingProfileTitle settingTitle">Настройки трансляции</p>
            <div className="SettingProfileBlock">
                <div className="SettingProfileBlockPhoto">
                    <div className="SettingProfileTitle SettingProfileTitleName">
                        <p>Название трансляциии</p>
                    </div>
                    <div className="SettingProfileBlockEdit">
                        <input type="text" className="SettingProfileBlockNameUser" placeholder="dmitriy_dota"
                               onChange={onChange}
                               value={userInfo.name}
                               multiple
                               name="name"/>
                        <p className="SettingProfileBlockEditInfo">Вы можете обновить имя по своему желанию</p>
                    </div>
                </div>
                <div className="SettingProfileBlockPhoto">
                    <div className="SettingProfileTitle SettingProfileTitleName">
                        <p>О себе</p>
                    </div>
                    <div className="SettingProfileBlockEdit">
                        <textarea type="text" style={{height: "114px", resize: "none"}}
                                  className="SettingProfileBlockNameUser"
                                  placeholder="dmitriy_dota@gmail.com"
                                  value={(userInfo.description !== null) ? userInfo.description : " "}
                                  onChange={onChange}
                                  name="description"
                        />
                        <p className="SettingProfileBlockEditInfo">Текст для панели «Описание» на вашем канале должен
                            содержать не более 300 символов</p>
                    </div>
                </div>
                <div className="SettingProfileBlockPhoto">
                    <div className="SettingProfileTitle SettingProfileTitleName">
                        <p>Обложка трансляции</p>
                    </div>
                    <div className="SettingProfileBlockEdit">
                        <div className="input__wrapper">
                            <input type="file" name="file" id="input__file" className="input input__file"
                                   multiple/>
                            <label htmlFor="input__file" className="input__file-button">

                                <span className="input__file-button-text SettingProfileBlockPhotoImageEditButton">Изменить фотографию</span>
                            </label>
                        </div>
                        <p className="SettingProfileBlockPhotoImageEditText" style={{textAlign: "left"}}>Поддерживаемые
                            форматы: PNG, JPG или
                            GIF.</p>
                    </div>
                </div>
                <div style={{borderBottom: "1px solid #293858"}}>
                    <div className="SettingProfileBlockPhoto SettingProfileBlockPhotoBroadcastInfo ">
                        <div className="SettingProfileTitle SettingProfileTitleName">
                            <p style={{lineHeight: "160%"}}>Ключ основной трансляции</p>
                        </div>
                        <div className="SettingProfileBlockEdit SettingProfileBlockEditBroadcast">

                            <input style={{height: "30px"}} type="text" className="SettingProfileBlockNameUser"
                                   placeholder="*************************************"
                                   disabled
                                   value={(!userInfo.stream_key) ? "AS" : userInfo.stream_key}
                                   name="stream_key"
                                   onChange={onChange}
                            />
                            <p className="SettingProfileBlockBroadCastKeyButtonCopy" onClick={() => {
                                navigator.clipboard.writeText(userInfo.stream_key)
                            }}>Копировать</p>
                            <p className="SettingProfileBlockBroadCastKeyButtonRefresh"
                               onClick={resetData}
                            >Сбросить</p>
                        </div>
                    </div>
                    <div className="SettingProfileBlockPhoto SettingProfileBlockPhotoBroadcastInfo">
                        <div className="SettingProfileTitle SettingProfileTitleName">
                            <p>Адресс сервера</p>
                        </div>
                        <div className="SettingProfileBlockEdit SettingProfileBlockEditBroadcast">
                            <input style={{height: "30px"}} type="text" className="SettingProfileBlockNameUser"
                                   placeholder="stream.gg/serverlink"
                                   value={userInfo.rtmp_server_url}
                                   onChange={onChange}
                                   name="rtmp_server_url"
                                   disabled
                            />
                            <i className="fac  " aria-hidden="true"><img src={quest} alt=""/></i>
                            <p className="SettingProfileBlockBroadCastKeyButtonCopy" onClick={() => {
                                navigator.clipboard.writeText(userInfo.rtmp_server_url)
                            }}>Копировать</p>
                        </div>
                    </div>
                </div>
                <div className="SettingProfileBlockPhoto">
                    <div className="SettingProfileTitle SettingProfileTitleName">
                        <p>Добавление категории</p>
                    </div>
                    <div className="SettingProfileBlockEdit">
                        <i className="fas  " aria-hidden="true"><img src={search} alt=""/></i>
                        <input style={{height: "30px", paddingLeft: "40px"}} type="text"
                               className="SettingProfileBlockNameUser SettingProfileBlockNameUserInputCategory "
                               placeholder="Искать по категориям"
                               value={(userInfo.category)?userInfo.category:""}
                               onChange={onChange}
                               name="category"
                        />
                    </div>
                </div>
                <div className="SettingProfileBlockPhoto">
                    <div className="SettingProfileTitle SettingProfileTitleName">
                        <p>Добавление тегов</p>
                    </div>
                    <div className="SettingProfileBlockEdit">
                        <i className="fas  " aria-hidden="true"><img src={search} alt=""/></i>
                        <input style={{height: "30px", paddingLeft: "40px"}} type="text"
                               className="SettingProfileBlockNameUser"
                               placeholder="Искать по тегам"
                            // value={userInfo.tsb}
                               onChange={onChange}
                               name="category"

                        />
                    </div>
                </div>
                <div className="SettingProfileBlockPhoto">
                    <div className="SettingProfileTitle SettingProfileTitleName">
                        <p>Обложка профиля</p>
                    </div>
                    <div className="SettingProfileBlockEdit">
                        <div className="input__wrapper">
                            <input type="file" name="file" id="input__file" className="input input__file"
                                   multiple/>
                            <label htmlFor="input__file" className="input__file-button">

                                <span className="input__file-button-text SettingProfileBlockPhotoImageEditButton">Изменить фотографию</span>
                            </label>
                        </div>
                        <p className="SettingProfileBlockPhotoImageEditText"
                           style={{textAlign: "left"}}>Отображается, когда ваш стрим оффлайн.
                            Поддерживаемые форматы: PNG, JPG или GIF.</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SettingBroadcast