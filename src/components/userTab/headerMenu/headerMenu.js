import React from "react";
import searchImage from "../../../image/search.png"

export function HeaderMenu() {
    return (
        <div className="menu" style={{display: "flex"}}>
            <a href="#">Просмотр</a>
            <a href="#">Киберспорт</a>
            <a href="#">Музыка</a>
        </div>
    )
}