import React, {useState} from "react";
import like from "../../image/like.svg"
import SingInAuthorizationBlock from "../../components/singInAuthorizationBlock/singInAuthorizationBlock";

function Footer() {
    const [modalBar, setModalBar] = useState(false)
    const regist = (e) => {
        if (!modalBar) {
            setModalBar(true)
        } else {
            setModalBar(false)
        }
    }
    return (

        <div className="footer">
            <div className="footerInfo">

                <img src={like} alt=""/>

                <p>Присоединяйся к нашему сообществу!
                    Лушие прямые трансляции в любой точке земного шара.</p>
            </div>
            <div className="footerButton">
                <a href="#" onClick={regist}>Регистрация</a>
            </div>
            {(modalBar)?<SingInAuthorizationBlock
                toggleBlock={regist}
                numberTab="2"
            />:null}
        </div>
    )

}

export default Footer