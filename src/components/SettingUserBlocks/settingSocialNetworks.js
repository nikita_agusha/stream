import React, {useEffect, useState} from "react";
import twitter from "../../image/twitter.svg";
import discord from "../../image/discord.svg"
import axios from "axios";
import {loadFromLocalStorage, saveToLocalStorage} from "../../LocalStorage";


function SettingSocialNetworks(props) {
    const [netWorks, setNetWorks] = useState(props.userData)
    const [token, setToken] = useState(loadFromLocalStorage().auth_token)
    const BASE_URL = window.env.ENV_VARIABLE;

    useEffect(() => {
        axios.get(BASE_URL + "/users/me", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        ).then(r => {
            setNetWorks(r.data.social_networks)
        })
    }, [])

    const onChange = (e) => {
        e.preventDefault()
        setNetWorks({...netWorks, [e.target.name]: e.target.value})
    }

    const editNetWorks = () => {
        axios.post(BASE_URL + "/users/me", {
                social_networks: netWorks
            }, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        ).then(r => {
            setNetWorks(r.data.data.social_networks)
        })
    }
    console.log(netWorks)
    const [image, setImage] = useState("")
    return (
        <div className="SettingProfile  ">
            <div className="SettingProfileBlock settingSocialNetworks">
                <p className="SettingProfileTitle settingTitle">Подключить социальные сети </p>
                <div className="SettingProfileNetworks">
                    <div className="SettingProfileNetwork">
                        {
                            (netWorks && netWorks.twitter) ?
                                <div className="SettingProfileNetworksImage">
                                    <img src={twitter} alt=""/>
                                </div>
                                :
                                <div className="SettingProfileNetworksImage  SettingProfileDontNetworks"/>
                        }
                        <div
                            className={(netWorks && netWorks.twitter) ? "SettingProfileNetworksText" : "SettingProfileNetworksText SettingProfileDontNetworks"}>
                            <p>Twitter</p>
                            <p>Социальная сеть</p>
                        </div>
                        <div className="SettingProfileNetworksInput">
                            <input type="text"
                                   className="SettingProfileBlockNameUser"
                                   name="twitter"
                                   placeholder="http://twitter/"
                                   value={netWorks && netWorks.twitter ? netWorks.twitter : ""}
                                   onChange={onChange}/>
                        </div>
                    </div>
                </div>
                <div className="SettingProfileNetworks">
                    <div className="SettingProfileNetwork">
                        {
                            (netWorks && netWorks.discord) ?
                                <div className="SettingProfileNetworksImage DiscordStyle">
                                    <img src={discord} alt=""/>
                                </div>
                                :
                                <div className="SettingProfileNetworksImage  SettingProfileDontNetworks"/>
                        }
                        <div
                            className={(netWorks && netWorks.discord) ? "SettingProfileNetworksText DiscordStyle " : "SettingProfileNetworksText   SettingProfileDontNetworks"}>
                            <p>Discord</p>
                            <p>Социальная сеть</p>
                        </div>
                        <div className="SettingProfileNetworksInput">
                            <input type="text"
                                   name="discord"
                                   placeholder="http://discord.com/"
                                   value={netWorks && netWorks.discord ? netWorks.discord : ""}
                                   onChange={onChange}
                                   className="SettingProfileBlockNameUser"/>
                        </div>
                    </div>
                </div>
                <div className="SettingProfileNetworks">
                    <div className="SettingProfileNetwork">
                        {
                            (netWorks && netWorks.Vk) ?
                                <div className="SettingProfileNetworksImage  ">
                                    <img src={twitter} alt=""/>
                                </div>
                                :
                                <div className="SettingProfileNetworksImage  SettingProfileDontNetworks"/>
                        }
                        <div
                            className={(netWorks && netWorks.vk) ? "SettingProfileNetworksText" : "SettingProfileNetworksText SettingProfileDontNetworks"}>
                            <p>Vk</p>
                            <p>Социальная сеть</p>
                        </div>
                        <div className="SettingProfileNetworksInput">
                            <input type="text"
                                   placeholder="http://Vk.com/"
                                   value={netWorks && netWorks.vk ? netWorks.vk : ""}
                                   onChange={onChange}
                                   name="vk"
                                   className="SettingProfileBlockNameUser"/>
                        </div>
                    </div>
                </div>
                <div className="SettingProfileNetworks">
                    <div className="SettingProfileNetwork">
                        {
                            (netWorks && netWorks.steam) ?
                                <div className="SettingProfileNetworksImage  ">
                                    <img src={twitter} alt=""/>
                                </div>
                                :
                                <div className="SettingProfileNetworksImage  SettingProfileDontNetworks"/>
                        }

                        <div
                            className={(netWorks && netWorks.steam) ? "SettingProfileNetworksText" : "SettingProfileNetworksText SettingProfileDontNetworks"}
                        >
                            <p>Steam</p>
                            <p>Социальная сеть</p>
                        </div>
                        <div className="SettingProfileNetworksInput">
                            <input type="text"
                                   className="SettingProfileBlockNameUser"
                                   placeholder="http://steam.com/"
                                   value={(netWorks && netWorks.steam) ? netWorks.steam : ""}
                                   name="steam"
                                   onChange={onChange}
                            />
                        </div>
                    </div>
                </div>
                <p onClick={editNetWorks} className="SettingProfileBlockPhotoImageEditButton"
                   style={{marginTop: "24px"}}>
                    Сохранить
                </p>
            </div>
        </div>
    )
}


export default SettingSocialNetworks