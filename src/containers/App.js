import React, {useEffect} from "react";
import './App.css';
import Containers from "./container/Container";
import "../style/reset.css"
import {Router, Route, BrowserRouter, Switch} from 'react-router-dom';
import history from "../history";
import PlayerVideo from "./PlayerVideo/PlayerVideo";
import UserTab from "../components/userTab/userTab";
import SettingUser from "./settingUser/settingUser";


function App() {


    useEffect(() => {
        document.title = `BroadCast`;
    });
    return (
        <>

            <div className="App" style={{margin: "0 auto", color: "#C1C1C1"}}>

                <BrowserRouter >
                    <UserTab   />
                    <Switch>
                        <Route path="/" exact component={Containers}/>
                        <Route path="/videoLink/:id" exact component={PlayerVideo}/>
                        <Route path="/settingUser"   exact component={SettingUser}/>
                    </Switch>
                </BrowserRouter>
            </div>
        < />
    )
}

export default App;
