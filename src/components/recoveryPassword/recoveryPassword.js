import React from "react";


const RecoveryPassword = (props) => {
    return (
        <div className="modalTab" onClick={() => (props.closemodal)}>
            <div className="recoveryBlock" onClick={(e) => e.stopPropagation()}>
                <h4>Восстановление пароля </h4>
                <p>Введите в форме ниже эл. адрес,<br/> к которому был привязан аккаунт
                </p>
                <input type="text"/>
                <a href="#"
                   className="recoveryButton" onClick={props.toggleButton}> Восстановить </a>
            </div>
        </div>
    )
}
export default RecoveryPassword