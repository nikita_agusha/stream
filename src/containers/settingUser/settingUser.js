import React from "react";
import SettingProfile from "../../components/SettingUserBlocks/settingProfile";
import SettingBroadcast from "../../components/SettingUserBlocks/settingBroadcast";
import SettingSocialNetworks from "../../components/SettingUserBlocks/settingSocialNetworks";
import {loadFromLocalStorage} from "../../LocalStorage";
import {Redirect} from "react-router";
import Container from "../container/Container";


function SettingUser() {
    if (loadFromLocalStorage() === undefined) {
        return <Redirect to={<Container/>}/>
    }
    return (
        <div className="SettingUser">
            <div className="SettingUserBlocks">
                <SettingProfile/>
                <SettingBroadcast/>
                <SettingSocialNetworks/>
            </div>
        </div>
    )
}

export default SettingUser