import InputEmoji from "react-input-emoji";
import letter from "../../image/letter.svg";
import React, {useState} from "react";
import SingInAuthorizationBlock from "../singInAuthorizationBlock/singInAuthorizationBlock";


function ChatPlayerVideo() {
    const [text, setText] = useState('')
    const [user, setUser] = useState("")
    const [modalBar, setModalBar] = useState(false)
    const regist = (e) => {
        if (!modalBar) {
            setModalBar(true)
        } else {
            setModalBar(false)
        }
    }

    function handleOnEnter(text) {
        console.log('enter', text)
    }
    return (
        <div className="chatBlock">
            <div className="chatBlockInput">
                {!user ? <p className="chatBlockNonSingIn">Вы не зарегистированны,
                    <span onClick={regist}>зарегестируйтесь</span>, чтобы начать
                    общение</p> : null}
                <InputEmoji
                    value={text}
                    onChange={setText}
                    cleanOnEnter
                    onEnter={handleOnEnter}
                    placeholder="Введите сообщение"
                >

                </InputEmoji>
                <a className="chatBlockInputButton" href="#"><img style={{verticalAlign: "-webkit-baseline-middle"}}
                                                                  src={letter} alt=""/></a>
            </div>
            {(modalBar)?<SingInAuthorizationBlock
                toggleBlock={regist}
                numberTab="2"
            />:null}
        </div>
    )
}

export default ChatPlayerVideo