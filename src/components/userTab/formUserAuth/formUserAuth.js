import React, {useEffect, useState} from "react";
import "../../../style/style.css"
import SingInAuthorizationBlock from "../../singInAuthorizationBlock/singInAuthorizationBlock";
import {NavLink} from "react-router-dom";
import {loadFromLocalStorage} from "../../../LocalStorage";
import axios from "axios";
import image from "../../../image/man.svg";


function FormUserAuth(props) {
    const [modalBar, setModalBar] = useState(false)
    const [tabNumber, setTabNumber] = useState("")
    const [user, setUser] = useState()
    const [token, setToken] = useState(loadFromLocalStorage())
    const BASE_URL = window.env.ENV_VARIABLE;

    useEffect(() => {
        if (token) {
            axios.get(BASE_URL + "/users/me", {
                headers:
                    {
                        Authorization: `Bearer ${token.auth_token}`
                    }
            }).then(r => {
                setUser(r.data)
            })
        }
    }, [])
    console.log(user)

    const toggle = (number) => {
        setTabNumber(number)
        if (!modalBar) {
            setModalBar(true)
        } else {
            setModalBar(false)
        }
    }


    const singOut = (e) => {
        e.preventDefault()
        setUser("")
    }

    return (
        <div className="blockAuth">
            {(modalBar) ?
                <SingInAuthorizationBlock
                    toggleBlock={toggle}
                    numberTab={tabNumber}
                    closeModal={setModalBar}
                />
                : null
            }
            {(user) ?
                <div className="userBlock">
                    <NavLink exact to={{
                        pathname: "/settingUser",
                        state: {user}
                    }} className="userBlockInfo">
                        {
                            (user.path_to_avatar !== null) ?
                                <img style={{width: "24px"}} src={user.path_to_avatar} alt=""/> :
                                <img src={image} alt=""/>
                        }
                        {user.username}</NavLink>
                </div>
                :
                <div className="blockAuthButtons">
                    <a href="#" className="singInButton" name="singIn" onClick={() => {
                        toggle("1");
                    }}
                    >Войти</a>
                    <a href="#" className="registrationButton" name="registr" onClick={() => {
                        toggle("2");
                    }}>Регистрация</a>
                </div>}

        </div>
    )
}


export default FormUserAuth