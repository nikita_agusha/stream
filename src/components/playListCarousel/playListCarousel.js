import React, {useEffect, useState} from "react";
import arrow from "../../image/arrow.svg"
import {Carousel} from '3d-react-carousal';
import videoPlay from "../../image/imageSlaider.svg"
import {NavLink} from "react-router-dom";
import man from "../../image/avatar.svg";
import axios from "axios";


function PlayListCarousel() {
    const Carousel = require('3d-react-carousal').Carousel;
    const [topVideo, setTopVideo] = useState("")
    const BASE_URL = window.env.ENV_VARIABLE;
    let loading = false
    useEffect(() => {
        loading = true
        axios.get(BASE_URL + "/broadcasts/top")
            .then(r => {
                setTopVideo(r.data)
                loading = false
            })
    }, [])
    console.log(topVideo)
    if (!loading) {
        console.log("Asdasd")
    }

    let slides = [
        <div>
            <p className="lifePeopleWatch"><span/> 12 343</p>
            <NavLink to={{pathname: `/videoLink/123`}}>

                <div className=" onlineBroadcast"
                     style={{backgroundImage: `url(${videoPlay})`}}>

                    <div className="carouselInfoBroadcastBlock">
                        <img className="channelPeopleCarousel" src={man} alt=""/>
                        <div>
                            <p className="carouselInfoBroadcastText">Стрим по доте два, розыгрышь скинов </p>
                            <p className="carouselInfoBroadcastText"> dmitriy_dota стримит Dota 2</p>
                        </div>
                    </div>
                </div>
            </NavLink>
        </div>, <div>
            <p className="lifePeopleWatch"><span/> 12 343</p>
            <NavLink to={{pathname: `/videoLink/123`}}>

                <div className=" onlineBroadcast"
                     style={{backgroundImage: `url(${videoPlay})`}}>

                    <div className="carouselInfoBroadcastBlock">
                        <img className="channelPeopleCarousel" src={man} alt=""/>
                        <div>
                            <p className="carouselInfoBroadcastText">Стрим по доте два, розыгрышь скинов </p>
                            <p className="carouselInfoBroadcastText"> dmitriy_dota стримит Dota 2</p>
                        </div>
                    </div>
                </div>
            </NavLink>
        </div>, <div>
            <p className="lifePeopleWatch"><span/> 12 343</p>
            <NavLink to={{pathname: `/videoLink/123`}}>

                <div className=" onlineBroadcast"
                     style={{backgroundImage: `url(${videoPlay})`}}>

                    <div className="carouselInfoBroadcastBlock">
                        <img className="channelPeopleCarousel" src={man} alt=""/>
                        <div>
                            <p className="carouselInfoBroadcastText">Стрим по доте два, розыгрышь скинов </p>
                            <p className="carouselInfoBroadcastText"> dmitriy_dota стримит Dota 2</p>
                        </div>
                    </div>
                </div>
            </NavLink>
        </div>, <div>
            <p className="lifePeopleWatch"><span/> 12 343</p>
            <NavLink to={{pathname: `/videoLink/123`}}>

                <div className=" onlineBroadcast"
                     style={{backgroundImage: `url(${videoPlay})`}}>

                    <div className="carouselInfoBroadcastBlock">
                        <img className="channelPeopleCarousel" src={man} alt=""/>
                        <div>
                            <p className="carouselInfoBroadcastText">Стрим по доте два, розыгрышь скинов </p>
                            <p className="carouselInfoBroadcastText"> dmitriy_dota стримит Dota 2</p>
                        </div>
                    </div>
                </div>
            </NavLink>
        </div>, <div>
            <p className="lifePeopleWatch"><span/> 12 343</p>
            <NavLink to={{pathname: `/videoLink/123`}}>

                <div className=" onlineBroadcast"
                     style={{backgroundImage: `url(${videoPlay})`}}>

                    <div className="carouselInfoBroadcastBlock">
                        <img className="channelPeopleCarousel" src={man} alt=""/>
                        <div>
                            <p className="carouselInfoBroadcastText">Стрим по доте два, розыгрышь скинов </p>
                            <p className="carouselInfoBroadcastText"> dmitriy_dota стримит Dota 2</p>
                        </div>
                    </div>
                </div>
            </NavLink>
        </div>, <div>
            <p className="lifePeopleWatch"><span/> 12 343</p>
            <NavLink to={{pathname: `/videoLink/123`}}>

                <div className=" onlineBroadcast"
                     style={{backgroundImage: `url(${videoPlay})`}}>

                    <div className="carouselInfoBroadcastBlock">
                        <img className="channelPeopleCarousel" src={man} alt=""/>
                        <div>
                            <p className="carouselInfoBroadcastText">Стрим по доте два, розыгрышь скинов </p>
                            <p className="carouselInfoBroadcastText"> dmitriy_dota стримит Dota 2</p>
                        </div>
                    </div>
                </div>
            </NavLink>
        </div>, <div>
            <p className="lifePeopleWatch"><span/> 12 343</p>
            <NavLink to={{pathname: `/videoLink/123`}}>

                <div className=" onlineBroadcast"
                     style={{backgroundImage: `url(${videoPlay})`}}>

                    <div className="carouselInfoBroadcastBlock">
                        <img className="channelPeopleCarousel" src={man} alt=""/>
                        <div>
                            <p className="carouselInfoBroadcastText">Стрим по доте два, розыгрышь скинов </p>
                            <p className="carouselInfoBroadcastText"> dmitriy_dota стримит Dota 2</p>
                        </div>
                    </div>
                </div>
            </NavLink>
        </div>,

    ]

    return (
        <div className="carousel">
            <Carousel slides={slides} autoplay={false} interval={3000} style={{background: "black"}}
                      className="carousel">
                <div className="slider-right">
                    <img src={arrow} alt=""/>
                </div>
                <div className="slider-left">
                    <img src={arrow} alt=""/>
                </div>
            </Carousel>
        </div>

    )
}

export default PlayListCarousel