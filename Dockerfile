FROM node:14.16.1-alpine3.12

COPY . /home/frontend

WORKDIR /home/frontend
RUN npm install

EXPOSE 3000

CMD [ "npm", "start" ]