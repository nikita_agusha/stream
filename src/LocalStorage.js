import {NotificationManager} from "react-notifications";

export const loadFromLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem("state");
        if (serializedState === null) {
            return;
        }
        return JSON.parse(serializedState);
    } catch (e) {
        return;
    }
};

export const saveToLocalStorage = state => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem("state", serializedState);
    } catch (e) {
        NotificationManager.error("Could not save state");
    }
};