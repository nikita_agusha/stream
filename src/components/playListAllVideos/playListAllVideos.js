import React from "react";
import videoPlay from "../../image/playListVideo.svg"
import man from "../../image/man.svg";
import CategoryToBeLike from "../categoryToBeLike/categoryToBeLike";
import {NavLink} from "react-router-dom";

function PlayListAllVideos() {
    const num= "123"
    return (
        <div className="  playListVideosLive">
            <p className="playListVideosLiveLabel"> Активные каналы, которые могут понравиться</p>
            <div className="liveBroadCastBlock">
                <div className="playListBlockAllVideosLive">
                    <div className=" playListAllVideoInfo ">
                        <p className="lifePeopleWatch"><span/> 12 343</p>
                        <NavLink to={{pathname: `/videoLink/${num}`}}>
                            <img className="lifeBroadcast" src={videoPlay} alt="" width="100%"/>
                        </NavLink>
                        <div className="playListVideoInfo">
                            <img className="channelPeople" src={man} alt=""/>
                            <div>
                                <p className="playListVideoInfoText">Стрим по доте два, розыгрыш </p>
                                <p className="playListVideoInfoText"><span
                                    className="playListVideoInfoTextName">dmitriy_dota </span> стримит Dota 2</p>
                            </div>
                        </div>
                        <div className="videoTags">

                            <p className="videoTag">Русский</p>
                            <p className="videoTag">Moba</p>
                        </div>

                    </div>
                </div>
                <div className="playListBlockAllVideosLive">
                    <div className=" playListAllVideoInfo ">
                        <p className="lifePeopleWatch"><span/> 12 343</p>
                        <NavLink to={{pathname: "/videoLink/:id"}}>
                            <img className="lifeBroadcast" src={videoPlay} alt="" width="100%"/>
                        </NavLink>
                        <div className="playListVideoInfo">
                            <img className="channelPeople" src={man} alt=""/>
                            <div>
                                <p className="playListVideoInfoText">Стрим по доте два, розыгрыш </p>
                                <p className="playListVideoInfoText"><span
                                    className="playListVideoInfoTextName">dmitriy_dota </span> стримит Dota 2</p>
                            </div>
                        </div>
                        <div className="videoTags">

                            <p className="videoTag">Русский</p>
                            <p className="videoTag">Moba</p>
                        </div>

                    </div>
                </div>
                <div className="playListBlockAllVideosLive">
                    <div className=" playListAllVideoInfo ">
                        <p className="lifePeopleWatch"><span/> 12 343</p>
                        <NavLink to={{pathname: "/videoLink/:id"}}>
                            <img className="lifeBroadcast" src={videoPlay} alt="" width="100%"/>
                        </NavLink>
                        <div className="playListVideoInfo">
                            <img className="channelPeople" src={man} alt=""/>
                            <div>
                                <p className="playListVideoInfoText">Стрим по доте два, розыгрыш </p>
                                <p className="playListVideoInfoText"><span
                                    className="playListVideoInfoTextName">dmitriy_dota </span> стримит Dota 2</p>
                            </div>
                        </div>
                        <div className="videoTags">

                            <p className="videoTag">Русский</p>
                            <p className="videoTag">Moba</p>
                        </div>

                    </div>
                </div>
                <div className="playListBlockAllVideosLive">
                    <div className=" playListAllVideoInfo ">
                        <p className="lifePeopleWatch"><span/> 12 343</p>
                        <NavLink to={{pathname: "/videoLink/:id"}}>
                            <img className="lifeBroadcast" src={videoPlay} alt="" width="100%"/>
                        </NavLink>
                        <div className="playListVideoInfo">
                            <img className="channelPeople" src={man} alt=""/>
                            <div>
                                <p className="playListVideoInfoText">Стрим по доте два, розыгрыш </p>
                                <p className="playListVideoInfoText"><span
                                    className="playListVideoInfoTextName">dmitriy_dota </span> стримит Dota 2</p>
                            </div>
                        </div>
                        <div className="videoTags">

                            <p className="videoTag">Русский</p>
                            <p className="videoTag">Moba</p>
                        </div>

                    </div>
                </div>
                <div className="playListBlockAllVideosLive">
                    <div className=" playListAllVideoInfo ">
                        <p className="lifePeopleWatch"><span/> 12 343</p>
                        <NavLink to={{pathname: `/videoLink/:id`}}>
                            <img className="lifeBroadcast" src={videoPlay} alt="" width="100%"/>
                        </NavLink>
                        <div className="playListVideoInfo">
                            <img className="channelPeople" src={man} alt=""/>
                            <div>
                                <p className="playListVideoInfoText">Стрим по доте два, розыгрыш </p>
                                <p className="playListVideoInfoText"><span
                                    className="playListVideoInfoTextName">dmitriy_dota </span> стримит Dota 2</p>
                            </div>
                        </div>
                        <div className="videoTags">

                            <p className="videoTag">Русский</p>
                            <p className="videoTag">Moba</p>
                        </div>

                    </div>
                </div>

            </div>
            {/* <ReactFlvPlayer
                url = "http://77.221.147.62:8080/live/first.flv"
                heigh = "800px"
                width = "800px"
                isMuted={true}
            />*/}
            <CategoryToBeLike/>
        </div>
    )
}

export default PlayListAllVideos