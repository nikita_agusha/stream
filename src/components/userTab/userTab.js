import React, {useEffect, useState} from "react";
import {HeaderMenu} from "./headerMenu/headerMenu";
import "../../style/style.css"
import Label from "./label/label";
import FormUserAuth from "./formUserAuth/formUserAuth";
import {loadFromLocalStorage} from "../../LocalStorage";
import axios from "axios";

function UserTab() {
    const [user, setUser] = useState(loadFromLocalStorage())
    const [token, setToken] = useState(loadFromLocalStorage())
    const BASE_URL = window.env.ENV_VARIABLE;
    useEffect(() => {
        if (token) {
            axios.get(BASE_URL + "/users/me", {
                headers:
                    {
                        Authorization: `Bearer ${token.auth_token}`
                    }
            }).then(r => {
                setUser(r.data)
            })
        }
    }, [])
    return (
        <div className="header">
          <div style={{display: "flex"}}>
                <Label/>
                <span className="headerLine" style={{
                    background: "linear-gradient(180deg, rgba(41, 56, 88, 0) 0%, #293858 51.69%, rgba(41, 56, 88, 0) 100%)",
                    display: "block",
                    marginTop: "6px"
                }}/>
                <HeaderMenu/>
            </div>
            <FormUserAuth user={user}/>
        </div>
    )
}

export default UserTab